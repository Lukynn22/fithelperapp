export const state = () => ({
  open: false,
  type: 'success',
  message: '',
});

export const mutations = {
  open(state, data) {
    state.open = true;
    state.message = data.message;
    state.type = data.type || 'success';
  },
  close(state) {
    state.open = false;
  },
};

export const actions = {
  open({commit}, data) {
    commit('open', data);
  },
  close({commit}) {
    commit('close');
  },
};
