import _ from 'lodash';

export const state = () => ({
  measurements: []
});


export const mutations = {
  set_measurements(state, measurements) {
    state.measurements = measurements;
  },
  add_measurement(state, measurement) {
    state.measurements.push(measurement);
  },
  update_measurement(state, measurementData) {
    state.measurements = _.map(state.measurements, (measurement) => {
      if (measurement._id === measurementData._id) {
        return measurementData;
      }
      return measurement;
    });
  },
  delete_measurement(state, measurementId) {
    state.measurements = _.filter(state.measurements, (measurement) => measurement._id !== measurementId);
  }
};

export const actions = {
  ensureMeasurements({dispatch, state}) {
    if (state.measurements.length > 0) {
      return Promise.resolve();
    }
    return dispatch('fetchMeasurements');
  },

  fetchMeasurements({commit, dispatch}) {
    return this.$axios.get(`/measurements`)
      .then(({ data }) => {
        commit('set_measurements', data);
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  getMeasurement({commit, state}, measurementId) {
    const measurement = _.find(state.measurements, (measurement) => measurement._id === measurementId);
    if (measurement) {
      return measurement;
    }

    return this.$axios.get(`/measurements/${measurementId}`)
      .then(({ data }) => {
        commit('add_measurement', data);
      })
      .catch(resp => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  createMeasurement({commit, rootState, dispatch}, data) {
    return this.$axios.post(`/measurements`, data)
      .then(({ data }) => {
        if (data.errors) {
          throw new Error();
        }
        commit('add_measurement', data);
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  updateMeasurement({commit, rootState, dispatch}, data) {
    return this.$axios.put(`/measurements/${data.measurementId}`, data)
      .then(({ data }) => {
        if (data.errors) {
          throw new Error();
        }
        commit('update_measurement', data);
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  deleteMeasurement({commit, dispatch}, measurementId) {
    return this.$axios.delete(`/measurements/${measurementId}`)
      .then(({ data }) => {
        if (data.errors) {
          throw new Error();
        }
        commit('delete_measurement', measurementId);
        dispatch('alert/open', {message: 'Measurement has been deleted', type: 'success'}, {root: true});
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  }
};

export const getters = {
  getMeasurements: state => {
    return state.measurements;
  },
};
