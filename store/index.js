import cookieparser from 'cookieparser';

export const actions = {
  nuxtServerInit({commit}, {req}) {
    let token = null;
    let userId = null;
    if (req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie);
      token = parsed.token;
      userId = parsed.userId;
    }
    commit('auth/auth_success', token || '');
    commit('auth/write_user', userId || null);
    if (token) {
      this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }
  },
};
