import VueCookie from 'vue-cookie';

export const state = () => ({
  status: '',
  token: '',
  userId: null,
});

export const mutations = {
  auth_request(state){
    state.status = 'loading';
  },
  auth_success(state, token) {
    state.status = 'success';
    state.token = token;
  },
  write_user(state, userId) {
    state.userId = userId;
  },
  auth_error(state) {
    state.status = 'error';
  },
  logout(state) {
    state.status = '';
    state.token = '';
    state.userId = null;
  },
};

export const actions = {
  login({commit, dispatch}, user) {
    return new Promise((resolve, reject) => {
      commit('auth_request');
      let params = new URLSearchParams();
      params.append('client_id', 'test');
      params.append('client_secret', 'testtest');
      params.append('grant_type', 'password');
      params.append('username', user.email);
      params.append('password', user.password);
      this.$axios.post('/oauth/token', params, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then(resp => {
          const token = resp.data.access_token;
          VueCookie.set('token', token);
          this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
          commit('auth_success', token);
          dispatch('alert/open', {message: 'You have been successfully logged', type: 'success'}, {root: true});
          resolve(resp);
        })
        .catch(err => {
          commit('auth_error');
          localStorage.removeItem('token');
          dispatch('alert/open', {message: 'Your email or password aren\'t matching', type: 'error'}, {root: true});
          reject(err);
        });
    });
  },
  register({commit}, user) {
    return new Promise((resolve, reject) => {
      commit('auth_request');
      this.$axios({url: 'http://localhost:3000/register', data: user, method: 'POST' })
        .then(resp => {
          const token = resp.data.token;
          localStorage.setItem('token', token);
          // Add the following line:
          this.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
          commit('auth_success', token);
          resolve(resp);
        })
        .catch(err => {
          commit('auth_error', err);
          localStorage.removeItem('token');
          reject(err);
        });
    });
  },
  me({commit, dispatch}) {
    return this.$axios.get('/me')
      .then(({ data }) => {
        if (data) {
          VueCookie.set('userId', data._id);
          commit('write_user', data._id);
        }
      })
      .catch(err => {
        commit('auth_error', err);
        localStorage.removeItem('token');
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },
  logout({commit, dispatch}) {
    return new Promise((resolve, reject) => {
      commit('logout');
      VueCookie.set('token', '');
      VueCookie.set('userId', '');
      delete this.$axios.defaults.headers.common['Authorization'];
      dispatch('alert/open', {message: 'Your have been logged out', type: 'success'}, {root: true});
      this.$router.push('/login');
      resolve();
    })
  },
};

export const getters = {
  isLoggedIn: state => !!state.token && !!state.userId,
  authStatus: state => state.status,
};
