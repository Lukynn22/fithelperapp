import _ from 'lodash';

export const state = () => ({
  weighings: []
});


export const mutations = {
  set_weighings(state, weighings) {
    state.weighings = weighings;
  },
  add_weighing(state, weighing) {
    state.weighings.push(weighing);
  },
  update_weighing(state, weighingData) {
    state.weighings = _.map(state.weighings, (weighing) => {
      if (weighing._id === weighingData._id) {
        return weighingData;
      }
      return weighing;
    });
  },
  delete_weighing(state, weighingId) {
    state.weighings = _.filter(state.weighings, (weighing) => weighing._id !== weighingId);
  }
};

export const actions = {
  ensureWeighings({dispatch, state}) {
    if (state.weighings.length > 0) {
      return Promise.resolve();
    }
    return dispatch('fetchWeighings');
  },

  fetchWeighings({commit, rootState, dispatch}) {
    return this.$axios.get(`/weighings/user/${rootState.auth.userId}`)
      .then(({ data }) => {
        commit('set_weighings', data);
      })
      .catch(resp => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  getWeighing({commit, state}, weighingId) {
    const weighing = _.find(state.weighings, (weighing) => weighing._id === weighingId);
    if (weighing) {
      return weighing;
    }

    return this.$axios.get(`/weighings/${weighingId}`)
      .then(({ data }) => {
        commit('add_weighing', data);
      })
      .catch(resp => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  createWeighing({commit, rootState, dispatch}, data) {
    return this.$axios.post(`/weighings/user/${rootState.auth.userId}`, data)
      .then(({ data }) => {
        if (data.errors) {
          throw new Error();
        }
        commit('add_weighing', data);
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  updateWeighing({commit, rootState, dispatch}, data) {
    return this.$axios.put(`/weighings/${data.weighingId}`, data)
      .then(({ data }) => {
        if (data.errors) {
          throw new Error();
        }
        commit('update_weighing', data);
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  },

  deleteWeighing({commit, dispatch}, weighingId) {
    return this.$axios.delete(`/weighings/${weighingId}`)
      .then(({ data }) => {
        if (data.errors) {
          throw new Error();
        }
        commit('delete_weighing', weighingId);
        dispatch('alert/open', {message: 'Weighing has been deleted', type: 'success'}, {root: true});
      })
      .catch(err => {
        console.log(err);
        dispatch('alert/open', {message: 'Error has occurred while processing', type: 'error'}, {root: true});
      });
  }
};

export const getters = {
  getWeighings: state => {
    return state.weighings;
  },
};
