export default function(req, res, next) {
  res.spa = false; // true without SSR

  next();
}
