export default function ({ $axios, redirect, store }) {
  $axios.onRequest(config => {
    config.headers.Authorization = `Bearer ${store.state.auth.token}`;
  });

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status);
    if (code === 401 || code === 400) {
      redirect('/login');
    }
  })
}
